require 'rails_helper'

RSpec.describe "vote_comments/edit", type: :view do
  before(:each) do
    @vote_comment = assign(:vote_comment, VoteComment.create!(
      :user_id => 1,
      :comment_id => 1
    ))
  end

  it "renders the edit vote_comment form" do
    render

    assert_select "form[action=?][method=?]", vote_comment_path(@vote_comment), "post" do

      assert_select "input#vote_comment_user_id[name=?]", "vote_comment[user_id]"

      assert_select "input#vote_comment_comment_id[name=?]", "vote_comment[comment_id]"
    end
  end
end
